package patchwork;

import java.util.ArrayList;
import java.util.List;


public class Timer extends Component implements TriggerSource
{
	public final FloatInlet interval;
	
	private int counter;
	private boolean wasTriggered;
	
	private final List<Triggerable> triggerables;

	public Timer(PatchContainer parent)
	{
		super(parent);
		getContainer().graph().setTerminal(this, true);
		
		interval = new FloatInlet(this);
		
		triggerables = new ArrayList<Triggerable>();
	}
	
	public Timer(float interval, PatchContainer parent)
	{
		this(parent);
		
		this.interval.set(interval);
	}

	@Override
	protected void process()
	{
		wasTriggered = false;
		
		if(counter++ == interval.floatValue())
		{
			wasTriggered = true;
			
			for(int i=0; i<triggerables.size(); i++)
			{
				triggerables.get(i).trigger();
			}
			
			counter = 0;
		}
	}
	
	public void patch(Triggerable t)
	{
		if(!triggerables.contains(t))
		{
			triggerables.add(t);
		}
	}
	
	public void patch(TriggerInlet triggerInlet)
	{
		PatchGraph.createObjectPatch(this, triggerInlet);
	}

	@Override
	public boolean wasTriggered()
	{
		return wasTriggered;
	}

}
