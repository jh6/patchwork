package patchwork;

import java.util.ArrayList;
import java.util.List;

import patchwork.signal.Signal;
import patchwork.signal.Signal.ChannelMode;

public class SignalInlet extends Inlet implements SignalValuable
{	
	Signal signal;
	
	private final List<SignalValuable> patched;
	private final List<FloatValuable> floatValuables;

	public SignalInlet(ChannelMode channelMode, Component parent)
	{
		super(parent);
		
		signal = Signal.obtain(channelMode);
		
		patched = new ArrayList<SignalValuable>();
		floatValuables = new ArrayList<FloatValuable>();
	}

	@Override
	protected boolean createPatch(Object obj)
	{
		if(obj instanceof SignalValuable)
		{
			patched.add((SignalValuable) obj);
			return true;
		}
		
		if(obj instanceof FloatValuable)
		{
			floatValuables.add((FloatValuable) obj);
			return true;
		}
		
		return false;
	}

	@Override
	protected void reset()
	{
		for(int i=0; i<signal.getNumChannels(); i++)
		{
			signal.set(i, 0.0f);
		}
	}

	@Override
	protected void tick()
	{
		for(int i=0; i<patched.size(); i++)
		{
			signal.add(patched.get(i).signal());
		}
		
		for(int i=0; i<floatValuables.size(); i++)
		{
			signal.add(floatValuables.get(i).floatValue());
		}
	}
	
	public float mono()
	{
		return signal.mono();
	}
	
	public float value(int channelIndex)
	{
		return signal.value(channelIndex);
	}
		
	public int numChannels()
	{
		return signal.getNumChannels();
	}
	
	@Override
	public Signal signal()
	{
		return signal;
	}
	
	public void patch(SignalInlet toInlet)
	{
		super.patch(toInlet);
	}
}
