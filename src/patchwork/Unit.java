package patchwork;

public abstract class Unit extends Component
{
	public Unit(PatchContainer parent)
	{
		super(parent);
	}
	
	public final Unit patch(Unit toUnit)
	{
		PatchGraph.createPatch(this, toUnit);
		return toUnit;
	}
	
	public final void patch(Inlet toInlet)
	{
		PatchGraph.createPatch(this, toInlet);
	}
	
	public final void patch(Dac dac)
	{
		PatchGraph.createPatch(this, dac);
	}
	
	public final void patch(Outlet moduleOutlet)
	{
		PatchGraph.createPatch(this, moduleOutlet);
	}
	
	protected final float sampleRate()
	{
		return Patchwork.INSTANCE.sampleRate();
	}
	
	public final void sink()
	{
		this.getContainer().graph().setTerminal(this, true);
	}
	
	public final void unsink()
	{
		this.getContainer().graph().setTerminal(this, false);
	}
}
