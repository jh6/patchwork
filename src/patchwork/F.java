package patchwork;


/**
 * Patchable float value.
 * @author Jack
 *
 */
public final class F implements FloatValuable
{
	private float value;
	
	public F() {}
	
	public F(float initialValue)
	{
		set(initialValue);
	}
	
	@Override
	public float floatValue()
	{
		return value;
	}

	public void set(float value)
	{
		this.value = value;
	}
	
	public void patch(FloatInlet toFloatInlet)
	{
		PatchGraph.createObjectPatch(this, toFloatInlet);
	}
	
	public void patch(SignalInlet toSignalInlet)
	{
		PatchGraph.createObjectPatch(this, toSignalInlet);
	}
	
	public void patch(Unit toUnit)
	{
		if(toUnit.defaultInlet != null)
		{
			if(toUnit.defaultInlet instanceof FloatInlet)
			{
				patch((FloatInlet) toUnit.defaultInlet);
			}
			else if(toUnit.defaultInlet instanceof SignalInlet)
			{
				patch((SignalInlet) toUnit.defaultInlet);
			}
			else
			{

			}
		}
		else
		{
			PW.err("Could not patch a float value to " + toUnit.getName());
		}
	}
}
