package patchwork.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;

import patchwork.Patchwork;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;

public class PWGUI
{

	/**
	 * Launch the application.
	 */
	public static void show(final Patchwork patchwork)
	{
		EventQueue.invokeLater(new Runnable()
		{
			
			public void run()
			{
				try
				{
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					
					PWGUI window = new PWGUI(patchwork);
					window.frame.setVisible(true);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	private JFrame frame;
	JButton startStopButton;
	
	private final Patchwork patchwork;

	private PWGUI(final Patchwork patchwork)
	{
		this.patchwork = patchwork;
		
		initialize();
		
		frame.addWindowListener(new WindowAdapter()
		{
			@Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent)
			{
				patchwork.stop();
				System.exit(0);
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("Patchwork");
		frame.setType(Type.UTILITY);
		frame.setBounds(100, 100, 112, 55);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		
		startStopButton = new JButton("Start");
		startStopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				if(patchwork.getIsRunning())
				{
					patchwork.stop();
				}
				else
				{
					patchwork.start();
				}
				
				validateStartStopButton();
			}
		});
		panel.add(startStopButton);
		validateStartStopButton();
	}
	
	private void validateStartStopButton()
	{
		if(patchwork.getIsRunning())
		{
			startStopButton.setText("Stop");
		}
		else
		{
			startStopButton.setText("Start");
		}
	}

}
