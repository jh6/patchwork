package patchwork.signal;

import patchwork.PW;

class StereoSignal extends Signal
{
	private final float[] values = new float[2];

	@Override
	public float mono()
	{
		return PW.stereoToMono(values[0], values[1]);
	}

	@Override
	public float value(int channelIndex)
	{
		return values[channelIndex];
	}
	
	@Override
	public void add(float toAllChannels)
	{
		values[0] += toAllChannels;
		values[1] += toAllChannels;
	}

	@Override
	public void set(float mono)
	{
		values[0] = mono;
		values[1] = mono;
	}

	@Override
	public void set(float left, float right)
	{
		values[0] = left;
		values[1] = right;
	}
	
	@Override
	public void set(int channelIndex, float value)
	{
		values[channelIndex] = value;
	}

	@Override
	public void add(Signal signal)
	{
		values[0] = signal.value(0);
		values[1] = signal.value(1);
	}

	@Override
	public int getNumChannels()
	{
		return 2;
	}

}
