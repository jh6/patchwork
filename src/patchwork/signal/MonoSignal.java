package patchwork.signal;

import patchwork.PW;

class MonoSignal extends Signal
{
	private float value;

	@Override
	public float mono()
	{
		return value;
	}

	@Override
	public float value(int channelIndex)
	{
		return value;
	}

	@Override
	public void set(float mono)
	{
		value = mono;
	}
	
	@Override
	public void set(float left, float right)
	{
		value = PW.stereoToMono(left, right);
	}
	
	@Override
	public void set(int channelIndex, float value)
	{
		this.value = value;
	}

	@Override
	public void add(float toAllChannels)
	{
		value += toAllChannels;
	}
	
	@Override
	public void add(Signal signal)
	{
		value += signal.mono();
	}

	@Override
	public int getNumChannels()
	{
		return 1;
	}


}
