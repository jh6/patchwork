package patchwork.signal;

/**
 * Represents a single sample frame of an audio signal.
 * @author Jack
 *
 */
public abstract class Signal
{
	public static enum ChannelMode
	{
		MONO(1),
		STEREO(2);
		
		public final int numChannels;
		ChannelMode(int numChannels)
		{
			this.numChannels = numChannels;
		}
	}
	
	public abstract float mono();
	public abstract float value(int channelIndex);
	
	public abstract void set(float mono);
	public abstract void set(float left, float right);
	public abstract void set(int channelIndex, float value);
	
	public abstract void add(float toAllChannels);
	public abstract void add(Signal signal);
	
	public abstract int getNumChannels();
	
	static public Signal obtain(ChannelMode channelMode)
	{
		switch(channelMode)
		{
		case MONO:
			return new MonoSignal();
		
		case STEREO:
			return new StereoSignal();
		}
		
		return null;
	}
}
