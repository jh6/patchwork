package patchwork;

public interface TriggerSource
{
	boolean wasTriggered();
}
