package patchwork;

public abstract class Outlet extends Let
{
	public Outlet(Component parent)
	{
		super(parent);
		
		parent.addOutlet(this);
	}
	
	protected abstract void reset();
	
	protected abstract boolean createPatch(Outlet outlet);
	
	public final void patch(Inlet toInlet)
	{
		PatchGraph.createPatch(this, toInlet);
	}
}
