package patchwork;

/**
 * Patchable boolean value.
 * @author Jack
 *
 */
public class B implements BooleanValuable
{
	private boolean state;

	public B() {}
	
	public B(boolean initialValue)
	{
		set(initialValue);
	}
	
	public void set(boolean state)
	{
		this.state = state;
	}
	
	public void patch(BooleanInlet toInlet)
	{
		PatchGraph.createObjectPatch(this, toInlet);
	}
	
	public void patch(Unit toUnit)
	{
		PatchGraph.createObjectPatch(this, toUnit);
	}
	
	@Override
	public boolean booleanValue()
	{
		return state;
	}

}
