package patchwork;

/**
 * AudioEngine represents the interface between Patchwork and the system's audio device.
 * @author Jack
 *
 */
public abstract class AudioEngine
{
	private Patchwork patchwork;

	final void init(Patchwork patchwork)
	{
		this.patchwork = patchwork;
	}
	
	/**
	 * Causes Patchwork to process a single sample frame of audio.
	 * 
	 * @param inputFrames an array containing float values from the system's audio input.
	 * @return an array containing the output of the dac, which should be sent to the system's audio output.
	 */
	protected final float[] process(float[] inputFrames)
	{
		patchwork.adc.setInput(inputFrames);
		patchwork.tick();
		
		return patchwork.dac.getOutput();
	}
	
	protected abstract void start();
	protected abstract void stop();
	
	protected abstract float sampleRate();
	protected abstract int numInputChannels();
	protected abstract int numOutputChannels();
}
