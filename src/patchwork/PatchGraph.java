package patchwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Stores the connections between PatchNodes.
 * 
 * @author Jack
 * 
 */
final class PatchGraph
{
	// Map of PatchNodes against a list of PatchNodes which must tick() before
	// them.
	private final Map<PatchNode, List<PatchNode>> dependencies;

	// List of PatchNodes at the end of the chain.
	private final List<PatchNode> terminalNodes;

	// List of Tickables in the order they must tick().
	private final List<PatchNode> chain;

	private boolean isValid;

	private final PatchContainer patch;

	PatchGraph(PatchContainer patch)
	{
		dependencies = new HashMap<PatchNode, List<PatchNode>>();
		terminalNodes = new ArrayList<PatchNode>();
		chain = new ArrayList<PatchNode>();

		this.patch = patch;
	}

	void tick()
	{
		validate();

		for(int i = chain.size() - 1; i > -1; i--)
		{
			//System.out.println(chain.get(i).getName());
			chain.get(i).tick();
		}
		
	//	System.out.println();
	}

	private void validate()
	{
		if(!isValid)
		{
			chain.clear();

			for(int i = 0; i < terminalNodes.size(); i++)
			{
				addToChain(terminalNodes.get(i));
			}

			isValid = true;
		}
	}

	private void addToChain(PatchNode node)
	{
		if(chain.contains(node))
		{
			return;
		}

		chain.add(node);


		// If the node has dependencies, add those to the chain.
		if(dependencies.containsKey(node))
		{
			List<PatchNode> n = dependencies.get(node);

			for(int i = 0; i < n.size(); i++)
			{
				addToChain(n.get(i));
			}
		}
	}

	void createDependency(PatchNode a, PatchNode b)
	{
		if(!dependencies.containsKey(a))
		{
			dependencies.put(a, new ArrayList<PatchNode>());
		}

		List<PatchNode> d = dependencies.get(a);

		d.add(b);

		isValid = false;
	}

	void setTerminal(PatchNode node, boolean isTerminal)
	{
		if(isTerminal)
		{
			if(!terminalNodes.contains(node))
			{
				terminalNodes.add(node);
			}
		}
		else
		{
			terminalNodes.remove(node);
		}
	}

	/*
	 * Static patching methods
	 */

	static void createPatch(Outlet o, Inlet i)
	{
		if(o.getContainer() != i.getContainer())
		{
			throw new RuntimeException(PWStrings.notInSameContainer(o, i));
		}

		if(!i.createPatch(o))
		{
			throw new RuntimeException(PWStrings.invalidPatch(o, i));
		}

		 o.getContainer().graph().createDependency(i, o);
		 System.out.println("PATCHED: " + o.getName() + "=>" + i.getName());
	}
	
	static void createPatch(Outlet o, Outlet moduleOutlet)
	{
		// moduleOutlet must be the outlet to a module
		if(!(moduleOutlet.parent instanceof Module))
		{
			throw new RuntimeException(PWStrings.notAModuleOutlet(o, moduleOutlet));
		}
		
		// moduleOutlet must the the outlet the the module that o's parent is in
		if(o.parent.getContainer() != moduleOutlet.parent)
		{
			throw new RuntimeException(PWStrings.outletNotInModule(o, moduleOutlet));
		}
		
		if(!moduleOutlet.createPatch(o))
		{
			throw new RuntimeException(PWStrings.invalidPatch(o, moduleOutlet));
		}
		
		o.getContainer().graph().setTerminal(o, true);
	}
	
	static void createPatch(Inlet moduleInlet, Inlet i)
	{
		// moduleInlet must be the inlet to a module
		if(!(moduleInlet.parent instanceof Module))
		{
			throw new RuntimeException(PWStrings.notAModuleInlet(moduleInlet, i));
		}
		
		// moduleInlet must the the inlet the the module that i's parent is in
		if(i.parent.getContainer() != moduleInlet.parent)
		{
			throw new RuntimeException(PWStrings.inletNotInModule(moduleInlet, i));
		}
		
		if(!i.createPatch(moduleInlet))
		{
			throw new RuntimeException(PWStrings.invalidPatch(moduleInlet, i));
		}
	}
	
	static void createPatch(Component c, Outlet moduleOutlet)
	{
		if(c.defaultOutlet == null)
		{
			throw new RuntimeException(PWStrings.noDefaultOutlet(c, moduleOutlet));
		}

		createPatch(c.defaultOutlet, moduleOutlet);
	}

	static void createPatch(Outlet o, Component c)
	{
		if(c.defaultInlet == null)
		{
			throw new RuntimeException(PWStrings.noDefaultInlet(o, c));
		}

		createPatch(o, c.defaultInlet);
	}

	static void createPatch(Component c, Inlet i)
	{
		if(c.defaultOutlet == null)
		{
			throw new RuntimeException(PWStrings.noDefaultOutlet(c, i));
		}

		createPatch(c.defaultOutlet, i);
	}

	static void createPatch(Component c, Component d)
	{
		if(c.defaultOutlet == null)
		{
			throw new RuntimeException(PWStrings.noDefaultOutlet(c, d));
		}

		createPatch(c.defaultOutlet, d);
	}
	
	static void createObjectPatch(Object obj, Inlet inlet)
	{
		if(!inlet.createPatch(obj))
		{
			throw new RuntimeException(PWStrings.couldNotCreatePatch(obj.getClass().getSimpleName(), inlet.getName()));
		}
	}
	
	static void createObjectPatch(Object obj, Unit toUnit)
	{
		if(toUnit.defaultInlet == null)
		{
			throw new RuntimeException(PWStrings.noDefaultInlet(obj.getClass().getSimpleName(), toUnit.getName()));
		}
		
		createObjectPatch(obj, toUnit.defaultInlet);
	}
}
