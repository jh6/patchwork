package patchwork;

import java.util.HashMap;
import java.util.Map;

import patchwork.signal.Signal.ChannelMode;

public final class Dac extends Component
{
	private final SignalInlet inlet;
	private final Map<Integer, SignalInlet> channels;
	
	private float[] output;
	
	float masterGain = .2f;
	
	int numChannels;
	
	Dac(int numChannels, Patchwork pw)
	{
		super(pw);
		
		pw.graph().setTerminal(this, true);
		
		inlet = new SignalInlet(ChannelMode.STEREO, this);
		setDefaultInlet(inlet);
		
		channels = new HashMap<Integer, SignalInlet>();
		
		output = new float[numChannels];
	}
		
	float[] getOutput()
	{
		return output;
	}

	@Override
	protected void process()
	{
		for(int i=0; i<output.length; i++)
		{
			output[i] = 0.0f;
			
			if(i < 2)
			{
				output[i] = inlet.value(i) * masterGain;
			}
			
			if(channels.containsKey(i))
			{
				output[i] += channels.get(i).mono() * masterGain;
			}
		}
	}
	
	public SignalInlet channel(int channelIndex)
	{
		if(!channels.containsKey(channelIndex))
		{
			channels.put(channelIndex, new SignalInlet(ChannelMode.MONO, this));
		}
		
		return channels.get(channelIndex);
	}
}
