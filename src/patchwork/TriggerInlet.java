package patchwork;

import java.util.ArrayList;
import java.util.List;

public class TriggerInlet extends Inlet implements TriggerSource
{
	private final List<TriggerSource> patched;
	
	private boolean wasTriggered;

	public TriggerInlet(Component parent)
	{
		super(parent);
		
		patched = new ArrayList<TriggerSource>();
	}

	@Override
	protected boolean createPatch(Object obj)
	{
		if(obj instanceof TriggerSource)
		{
			patched.add((TriggerSource) obj);
			return true;
		}
		
		return false;
	}

	@Override
	protected void reset()
	{
		wasTriggered = false;
	}

	@Override
	protected void tick()
	{
		for(int i=0; i<patched.size(); i++)
		{
			if(patched.get(i).wasTriggered())
			{
				wasTriggered = true;
				return;
			}
		}
	}

	@Override
	public boolean wasTriggered()
	{
		return wasTriggered;
	}

}
