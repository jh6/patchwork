package patchwork;

public class Module extends Unit implements PatchContainer
{
	private final PatchGraph graph;

	public Module(PatchContainer parent)
	{
		super(parent);
		
		graph = new PatchGraph(this);
	}

	@Override
	protected void process()
	{
		graph.tick();
	}

	@Override
	public PatchGraph graph()
	{
		return graph;
	}

}
