package patchwork;

import java.util.ArrayList;
import java.util.List;

import patchwork.signal.Signal;
import patchwork.signal.Signal.ChannelMode;

public class SignalOutlet extends Outlet implements FloatValuable, SignalValuable
{
	Signal setSignal;
	Signal signal;
	
	private List<SignalOutlet> patched;

	public SignalOutlet(ChannelMode channelMode, Component parent)
	{
		super(parent);
		
		setSignal = Signal.obtain(channelMode);
		signal = Signal.obtain(channelMode);
		
		patched = new ArrayList<SignalOutlet>();
	}
	
	@Override
	protected void reset()
	{
		for(int i=0; i<setSignal.getNumChannels(); i++)
		{
			setSignal.set(i, 0.0f);
			signal.set(i, 0.0f);
		}
	}

	@Override
	protected void tick()
	{
		for(int i=0; i<patched.size(); i++)
		{
			signal.add(patched.get(i).setSignal);
		}
		
		signal.add(setSignal);
	}

	public void set(float mono)
	{
		setSignal.set(mono);
	}
	
	public void set(float left, float right)
	{
		setSignal.set(left, right);
	}
	
	public void set(float[] stereo)
	{
		setSignal.set(stereo[0], stereo[1]);
	}
	
	public void set(int channelIndex, float value)
	{
		setSignal.set(channelIndex, value);
	}
	
	public int numChannels()
	{
		return setSignal.getNumChannels();
	}

	@Override
	public float floatValue()
	{
		return setSignal.mono();
	}

	@Override
	protected boolean createPatch(Outlet toOutlet)
	{
		if(toOutlet instanceof SignalOutlet)
		{
			patched.add((SignalOutlet) toOutlet);
			return true;
		}
		
		return false;
	}
	
	public void patch(Dac toDac)
	{
		PatchGraph.createPatch(this, toDac);
	}
	
	public Unit patch(Unit toUnit)
	{
		PatchGraph.createPatch(this, toUnit);
		return toUnit;
	}

	@Override
	public Signal signal()
	{
		return signal;
	}
}
