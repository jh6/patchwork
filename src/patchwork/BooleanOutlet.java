package patchwork;

import java.util.ArrayList;
import java.util.List;

public class BooleanOutlet extends Outlet implements BooleanValuable
{
	private final List<BooleanValuable> patched;
	
	private boolean setState;
	private boolean state;

	public BooleanOutlet(Component parent)
	{
		super(parent);
		patched = new ArrayList<BooleanValuable>();
	}
	
	public void set(boolean value)
	{
		setState = value;
	}

	@Override
	protected void reset()
	{
		state = false;
	}

	@Override
	protected boolean createPatch(Outlet outlet)
	{
		if(outlet instanceof BooleanValuable)
		{
			patched.add((BooleanValuable) outlet);
			return true;
		}
		
		return false;
	}

	@Override
	protected void tick()
	{
		if(setState)
		{
			state = true;
			return;
		}
		
		for(int i=0; i<patched.size(); i++)
		{
			if(patched.get(i).booleanValue())
			{
				state = true;
				return;
			}
		}
	}

	@Override
	public boolean booleanValue()
	{
		return state;
	}
}
