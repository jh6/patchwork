package patchwork.units.stereo;

import patchwork.PatchContainer;
import patchwork.SignalInlet;
import patchwork.SignalOutlet;
import patchwork.Unit;
import patchwork.signal.Signal.ChannelMode;

public class MonoSum extends Unit
{
	private final SignalInlet in;
	private final SignalOutlet out;

	public MonoSum(PatchContainer parent)
	{
		super(parent);
		
		in = new SignalInlet(ChannelMode.STEREO, this);
		out = new SignalOutlet(ChannelMode.MONO, this);
		
		setDefaultInlet(in);
		setDefaultOutlet(out);
	}

	@Override
	protected void process()
	{
		out.set(in.mono());
	}
}
