package patchwork.units.generators;

import patchwork.PW;
import patchwork.PatchContainer;
import patchwork.SignalInlet;
import patchwork.SignalOutlet;
import patchwork.Unit;
import patchwork.Waveform;
import patchwork.signal.Signal.ChannelMode;

public class Oscillator extends Unit
{	
	public final SignalInlet frequency;
	public final SignalInlet phase;
	private final SignalOutlet out;
	
	private Waveform waveform;
	private float[] position;

	public Oscillator(Waveform waveform, ChannelMode channelMode, PatchContainer parent)
	{
		super(parent);
		
		frequency = new SignalInlet(channelMode, this);
		frequency.setName("frequency");
		phase = new SignalInlet(channelMode, this);
		phase.setName("phase");
		
		out = new SignalOutlet(channelMode, this);
		out.setName("output");
		setDefaultOutlet(out);
		
		this.waveform = waveform;
		position = new float[channelMode.numChannels];
	}
	
	public Oscillator(Waveform waveform, PatchContainer parent)
	{
		this(waveform, ChannelMode.MONO, parent);
	}

	@Override
	protected void process()
	{
		for(int i=0; i<out.numChannels(); i++)
		{
			float x = position[i];
			
//			if(x > 1f) { x -= 1f; }
//			if(x < 0f) { x += 1f; }
			
			out.set(i, waveform.get(x));

			position[i] += Math.max(0f, frequency.value(i)) / sampleRate();
			
			if(position[i] > 1f) { position[i] -= 1f; }
		}
	}
}
