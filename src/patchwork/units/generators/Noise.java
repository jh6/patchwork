package patchwork.units.generators;

import patchwork.PW;
import patchwork.PatchContainer;
import patchwork.SignalOutlet;
import patchwork.Unit;
import patchwork.signal.Signal.ChannelMode;

/**
 * Noise generator.<p>
 * 
 * Mono by default.
 * @author Jack
 *
 */
public class Noise extends Unit
{
	public final SignalOutlet out;

	public Noise(ChannelMode channelMode, PatchContainer parent)
	{
		super(parent);
		
		out = new SignalOutlet(channelMode, this);
		setDefaultOutlet(out);
	}
	
	public Noise(PatchContainer parent)
	{
		this(ChannelMode.MONO, parent);
	}

	@Override
	protected void process()
	{
		for(int i=0; i<out.numChannels(); i++)
		{
			out.set(i, PW.fRand(-1.0f, 1.0f));
		}
	}

}
