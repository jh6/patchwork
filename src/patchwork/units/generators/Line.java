package patchwork.units.generators;

import patchwork.FloatInlet;
import patchwork.FloatOutlet;
import patchwork.PW;
import patchwork.PatchContainer;
import patchwork.TriggerOutlet;
import patchwork.Triggerable;
import patchwork.Unit;

public class Line extends Unit implements Triggerable
{
	public final FloatInlet start;
	public final FloatInlet end;
	public final FloatInlet duration;
	
	private final FloatOutlet out;
	public final TriggerOutlet onComplete;
	
	private boolean isRunning;

	private float value;
	private float step;
	private float target;
	
	private int counter;
	private int thisDuration;

	public Line(PatchContainer parent)
	{
		super(parent);
		
		start = new FloatInlet(this);
		start.setName("start");
		end = new FloatInlet(this);
		end.setName("target");
		duration = new FloatInlet(this);
		duration.setName("duration");
		out = new FloatOutlet(this);
		out.setName("value");
		onComplete = new TriggerOutlet(this);
		onComplete.setName("onComplete");
		
		setDefaultOutlet(out);
	}

	@Override
	protected void process()
	{	
		if(isRunning)
		{
			value += step;
						
			if(counter++ == thisDuration)
			{
				onComplete.trigger();
				isRunning = false;
			}
			
		}
		
		out.set(value);
	}

	@Override
	public void trigger()
	{
		if(duration.floatValue() == 0f)
		{
			value = end.floatValue();
			isRunning = false;
			return;
		}
		
		value = start.floatValue();
		
		float d = end.floatValue() - value;
		step = d / duration.floatValue();
		
		thisDuration = (int) duration.floatValue();
		
		counter = 0;
		isRunning = true;
		
		System.out.println("target " + end.floatValue() + ", start " + value);
	}
}
