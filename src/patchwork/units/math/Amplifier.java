package patchwork.units.math;

import patchwork.PatchContainer;
import patchwork.SignalInlet;
import patchwork.SignalOutlet;
import patchwork.Unit;
import patchwork.signal.Signal.ChannelMode;

/**
 * Multiplier for Signals.
 * @author Jack
 *
 */
public class Amplifier extends Unit
{
	private final SignalInlet in;
	
	/**
	 * Signal to multiply by.
	 */
	public final SignalInlet multiplier;
	
	private final SignalOutlet out;

	public Amplifier(ChannelMode channelMode, PatchContainer parent)
	{
		super(parent);
		
		in = new SignalInlet(channelMode, this);
		multiplier = new SignalInlet(channelMode, this);
		out = new SignalOutlet(channelMode, this);
		
		setDefaultInlet(in);
		setDefaultOutlet(out);
	}
	
	public Amplifier(PatchContainer parent)
	{
		this(ChannelMode.STEREO, parent);
	}

	@Override
	protected void process()
	{
		for(int i=0; i<in.numChannels(); i++)
		{
			out.set(i, in.value(i) * multiplier.value(i));
		}
	}

}
