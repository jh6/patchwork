package patchwork.units.math;

import patchwork.FloatInlet;
import patchwork.FloatOutlet;
import patchwork.PW;
import patchwork.PatchContainer;
import patchwork.TriggerInlet;
import patchwork.Triggerable;
import patchwork.Unit;

/**
 * Generates a random float value.
 * @author Jack
 *
 */
public class FRandom extends Unit implements Triggerable
{
	public final FloatInlet min;
	public final FloatInlet max;
	
	private final FloatOutlet out;
	
	private float value;

	public FRandom(PatchContainer parent)
	{
		super(parent);
		
		min = new FloatInlet(this);
		min.setName("min");
		max = new FloatInlet(this);
		max.setName("max");
		
		out = new FloatOutlet(this);
		out.setName("out");
		
		setDefaultOutlet(out);
	}
	
	/**
	 * Construct and FRandom and set the min and max inlet values.
	 * @param min
	 * @param max
	 * @param parent
	 */
	public FRandom(float min, float max, PatchContainer parent)
	{
		this(parent);
		
		this.min.set(min);
		this.max.set(max);
	}

	@Override
	protected void process()
	{
		out.set(value);
	}

	@Override
	public void trigger()
	{
		value = PW.fRand(min.floatValue(), max.floatValue());
		System.out.println("fran" + value);
	}
}
