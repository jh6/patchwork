package patchwork.units.math;

import patchwork.FloatInlet;
import patchwork.FloatOutlet;
import patchwork.PatchContainer;
import patchwork.Unit;

/**
 * Multiplier for floats.
 * @author Jack
 *
 */
public class FMultiply extends Unit
{
	private final FloatInlet in;
	
	/**
	 * Value to multiply by.
	 */
	public final FloatInlet multiplier;
	
	private final FloatOutlet out;

	public FMultiply(PatchContainer parent)
	{
		super(parent);
		
		in = new FloatInlet(this);
		multiplier = new FloatInlet(this);
		
		out = new FloatOutlet(this);
		
		setDefaultInlet(in);
		setDefaultOutlet(out);
	}

	@Override
	protected void process()
	{
		out.set(in.floatValue() * multiplier.floatValue());
	}

}
