package patchwork.units;

import patchwork.PatchContainer;
import patchwork.SignalInlet;
import patchwork.SignalOutlet;
import patchwork.Unit;
import patchwork.signal.Signal.ChannelMode;

public class StereoSplitter extends Unit
{
	private final SignalInlet in;
	
	public final SignalOutlet left;
	public final SignalOutlet right;

	public StereoSplitter(PatchContainer parent)
	{
		super(parent);
		
		in = new SignalInlet(ChannelMode.STEREO, this);
		left = new SignalOutlet(ChannelMode.MONO, this);
		right = new SignalOutlet(ChannelMode.MONO, this);
	}

	@Override
	protected void process()
	{
		left.set(in.value(0));
		right.set(in.value(1));
	}
}
