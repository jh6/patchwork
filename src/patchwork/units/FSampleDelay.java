package patchwork.units;

import patchwork.FloatInlet;
import patchwork.FloatOutlet;
import patchwork.PatchContainer;
import patchwork.Unit;

public class FSampleDelay extends Unit
{
	private final FloatInlet in;
	private final FloatOutlet out;
	
	float value;

	public FSampleDelay(PatchContainer parent)
	{
		super(parent);
		
		in = new FloatInlet(this);
		out = new FloatOutlet(this);
		
		setDefaultInlet(in);
		setDefaultOutlet(out);
	}

	@Override
	protected void process()
	{
		out.set(value);
		value = in.floatValue();
	}

}
