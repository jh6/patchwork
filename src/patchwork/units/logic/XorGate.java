package patchwork.units.logic;

import patchwork.BooleanInlet;
import patchwork.BooleanOutlet;
import patchwork.PatchContainer;
import patchwork.Unit;

public class XorGate extends Unit
{
	public final BooleanInlet a;
	public final BooleanInlet b;
	private final BooleanOutlet out;

	public XorGate(PatchContainer parent)
	{
		super(parent);
		
		a = new BooleanInlet(this);
		b = new BooleanInlet(this);
		
		out = new BooleanOutlet(this);
		
		setDefaultOutlet(out);
	}

	@Override
	protected void process()
	{
		if(a.booleanValue() != b.booleanValue())
		{
			out.set(true);
		}
	}

}
