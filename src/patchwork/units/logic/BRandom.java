package patchwork.units.logic;

import patchwork.BooleanOutlet;
import patchwork.PW;
import patchwork.PatchContainer;
import patchwork.Unit;

public class BRandom extends Unit
{
	private final BooleanOutlet out;
	
	private boolean state;

	public BRandom(PatchContainer parent)
	{
		super(parent);
		
		out = new BooleanOutlet(this);
		
		setDefaultOutlet(out);
	}

	@Override
	protected void process()
	{
		state = PW.bRand();
		System.out.println(state);
		out.set(state);
	}

}
