package patchwork.units.logic;

import patchwork.BooleanInlet;
import patchwork.BooleanOutlet;
import patchwork.PatchContainer;
import patchwork.Unit;

public class AndGate extends Unit
{
	private final BooleanInlet in;
	private final BooleanOutlet out;

	public AndGate(PatchContainer parent)
	{
		super(parent);
		
		in = new BooleanInlet(this);
		out = new BooleanOutlet(this);
		
		setDefaultInlet(in);
		setDefaultOutlet(out);
	}

	@Override
	protected void process()
	{
		out.set(in.andValue());
	}

}
