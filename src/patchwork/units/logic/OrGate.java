package patchwork.units.logic;

import patchwork.BooleanInlet;
import patchwork.BooleanOutlet;
import patchwork.PatchContainer;
import patchwork.Unit;

public class OrGate extends Unit
{
	private final BooleanInlet in;
	private final BooleanOutlet out;

	public OrGate(PatchContainer parent)
	{
		super(parent);
		
		in = new BooleanInlet(this);
		out = new BooleanOutlet(this);
		
		setDefaultInlet(in);
		setDefaultOutlet(out);
	}

	@Override
	protected void process()
	{
		out.set(in.booleanValue());
	}
}
