package patchwork.units.logic;

import patchwork.BooleanInlet;
import patchwork.BooleanOutlet;
import patchwork.PatchContainer;
import patchwork.Unit;

public class NotGate extends Unit
{
	private final BooleanInlet in;
	private final BooleanOutlet out;
	
	public NotGate(PatchContainer parent)
	{
		super(parent);
		
		in = new BooleanInlet(this);
		out = new BooleanOutlet(this);
	}
	
	
	@Override
	protected void process()
	{
		out.set(!in.booleanValue());
	}
}
