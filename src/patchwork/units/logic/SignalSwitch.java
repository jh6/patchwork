package patchwork.units.logic;

import patchwork.BooleanInlet;
import patchwork.PatchContainer;
import patchwork.SignalInlet;
import patchwork.SignalOutlet;
import patchwork.Unit;
import patchwork.signal.Signal.ChannelMode;

public class SignalSwitch extends Unit
{

	private final SignalInlet in;
	public final BooleanInlet state;
	private final SignalOutlet out;
	
	public SignalSwitch(ChannelMode channelMode, PatchContainer parent)
	{
		super(parent);
		
		in = new SignalInlet(channelMode, this);
		state = new BooleanInlet(this);
		out = new SignalOutlet(channelMode, this);
		
		setDefaultInlet(in);
		setDefaultOutlet(out);
	}
	
	@Override
	protected void process()
	{
		if(state.booleanValue())
		{
			for(int i=0; i<in.numChannels(); i++)
			{
				out.set(i, in.value(i));
			}
		}
	}

}
