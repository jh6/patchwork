package patchwork;


public class SpamUnit extends Unit
{
	public final FloatInlet inlet;
	public final FloatOutlet outlet;

	public SpamUnit(PatchContainer parent)
	{
		super(parent);
		
		inlet = new FloatInlet(this);
		outlet = new FloatOutlet(this);
	}

	@Override
	protected void process()
	{
		outlet.set(inlet.floatValue());
		
		System.out.println("TICK " + getName() + " " + inlet.floatValue());
	}

}
