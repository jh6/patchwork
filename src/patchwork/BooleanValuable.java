package patchwork;

public interface BooleanValuable
{
	boolean booleanValue();
}
