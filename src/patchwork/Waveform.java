package patchwork;

public interface Waveform
{
	/**
	 * Return the value of the waveform at x, where x is a value between 0 and 1.
	 * @param x position
	 * @return value
	 */
	float get(float x);
	
	public static final Waveform SINE = new Waveform()
	{
		@Override
		public float get(float x)
		{
			return PW.sin(PW.TWO_PI * x);
		}
	};
	
	public static final Waveform SQUARE = new Waveform()
	{
		@Override
		public float get(float x)
		{
			if(x > .5f)
			{
				return 1f;
			}
			
			return 0f;
		}
	};
	
	public static final Waveform SAWTOOTH = new Waveform()
	{
		@Override
		public float get(float x)
		{
			return PW.range(x, 0, 1, -1f, 1f);
		}
	};
	
	public static final Waveform TRIANGLE = new Waveform()
	{
		@Override
		public float get(float x)
		{
			if(x > .5f)
			{
				return PW.range(x, 0f, .5f, -1f, 1f);
			}
			
			return PW.range(x, .5f, 1f, 1f, -1f);
		}
	};
}
