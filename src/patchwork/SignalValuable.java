package patchwork;

import patchwork.signal.Signal;

public interface SignalValuable
{
	Signal signal();
}
