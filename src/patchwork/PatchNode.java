package patchwork;

abstract class PatchNode extends Tickable
{
	private String name;
	
	private final PatchContainer container;
	
	PatchNode(PatchContainer patch)
	{
		this.container = patch;
	}
	
	final PatchContainer getContainer()
	{
		return container;
	}
	
	
	public void setName(String newName)
	{
		name = newName;
	}
	
	public String getName()
	{
		if(name == null)
		{
			name = getDefaultName();
		}
		
		return name;
	}
	
	String getDefaultName()
	{
		return "Unnamed " + getClass().getSimpleName();
	}
}
