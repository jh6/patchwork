package patchwork;

abstract class Tickable
{
	abstract void tick();
}
