package patchwork;

import java.util.ArrayList;
import java.util.List;

public class BooleanInlet extends Inlet implements BooleanValuable
{
	private List<BooleanValuable> patched;
	
	private boolean state;
	private int setState = -1; // -1 = unset, 0 = false, 1 = true;

	public BooleanInlet(Component parent)
	{
		super(parent);
		
		patched = new ArrayList<BooleanValuable>();
	}

	@Override
	protected boolean createPatch(Object obj)
	{
		if(obj instanceof BooleanValuable)
		{
			addBooleanValuable((BooleanValuable) obj);
			return true;
		}
		
		return false;
	}
	
	void addBooleanValuable(BooleanValuable v)
	{
		if(!patched.contains(v))
		{
			patched.add(v);
		}
	}

	@Override
	protected void reset()
	{
		state = false;
	}

	@Override
	protected void tick()
	{
		if(setState == 1)
		{
			state = true;
			return;
		}
		
		for(int i=0; i<patched.size(); i++)
		{
			if(patched.get(i).booleanValue())
			{
				state = true;
				return;
			}
		}
	}
	
	public void set(boolean b)
	{
		if(b) { setState = 1; }
		else { setState = 0; }
	}
	
	public void unset()
	{
		setState = -1;
	}

	/**
	 * Returns the OR value of all boolean values patched to this inlet.
	 */
	@Override
	public boolean booleanValue()
	{
		return state;
	}
		
	public boolean andValue()
	{
		if(setState == 0)
		{
			return false;
		}
		
		if(patched.size() == 0)
		{
			return false;
		}
		
		for(int i=0; i<patched.size(); i++)
		{
			if(!patched.get(i).booleanValue())
			{
				return false;
			}
		}
		
		return true;
	}
}
