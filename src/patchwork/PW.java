package patchwork;

import java.util.Random;

import patchwork.signal.Signal.ChannelMode;

/**
 * Utilities!
 * @author Jack
 *
 */
public class PW
{
	static public final float PI = 3.14159265359f;
	static public final float TWO_PI = 6.28318530718f;

	static protected final ChannelMode MONO = ChannelMode.MONO;
	static protected final ChannelMode STEREO = ChannelMode.STEREO;
	
	static protected final Waveform SINE = Waveform.SINE;
	
	/*
	 * Randomness
	 */
	
	static private final Random RANDOM = new Random();
	
	static public void setRandomSeed(long seed)
	{
		RANDOM.setSeed(seed);
	}
	
	/**
	 * Returns a random float between 0 and 1.
	 * @return random float.
	 */
	static public float fRand()
	{
		return RANDOM.nextFloat();
	}
	
	/**
	 * Returns a random float between 0 and max.
	 * @param max
	 * @return
	 */
	static public float fRand(float max)
	{		
		return RANDOM.nextFloat() * max;
	}
	
	/**
	 * Returns a random float between min and max.
	 * @param min
	 * @param max
	 * @return
	 */
	static public float fRand(float min, float max)
	{
		return range(RANDOM.nextFloat(), 0.0f, 1.0f, min, max);
	}
	
	static public boolean bRand()
	{
		return RANDOM.nextBoolean();
	}
	
	/*
	 * Math
	 */
	
	static public float sin(float x)
	{
		return (float) Math.sin(x);
	}

	static public float range(float value, float inMin, float inMax,
			float outMin, float outMax)
	{
		return outMin + (outMax - outMin)
				* ((value - inMin) / (inMax - inMin));
	}
		
	static public float clip(float value, float min, float max)
	{
		return Math.min(max, Math.max(value, min));
	}
	
	public static float lerp(float value, float target, float alpha)
	{
		float invAlpha = 1.0f - alpha;
		
		return value * invAlpha + target * alpha;
	}
	
	private static final float EPSILON =  0.0000001f;
		
	public static boolean isEqualEnough(float a, float b)
	{
		if(a == b) { return true; }
		
		float e = Math.abs(a - b);
		System.out.println("WE " + (e < EPSILON));
		return e < EPSILON;
	}
	
	/*
	 * Stereo
	 */
		
	static public float stereoToMono(float left, float right)
	{
		return (left + right) * 0.5f;
	}
	
	static void err(String message)
	{
		System.err.println("PW: " + message);
	}
}
