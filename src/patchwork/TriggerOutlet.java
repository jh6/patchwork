package patchwork;

import java.util.ArrayList;
import java.util.List;

public class TriggerOutlet extends Outlet implements TriggerSource
{
	private boolean wasTriggered;
	
	private final List<TriggerOutlet> patched;
	private final List<Triggerable> triggerables;

	public TriggerOutlet(Component parent)
	{
		super(parent);

		patched = new ArrayList<TriggerOutlet>();
		triggerables = new ArrayList<Triggerable>();
	}

	@Override
	protected void reset()
	{
		wasTriggered = false;
	}
	
	/**
	 * Trigger the outlet. Should only be called from within the parent Unit's <code>process()</code> method.
	 */
	public void trigger()
	{
		wasTriggered = true;
	}

	@Override
	protected boolean createPatch(Outlet outlet)
	{
		if(outlet instanceof TriggerOutlet)
		{
			patched.add((TriggerOutlet) outlet);
			return true;
		}
		
		return false;
	}

	@Override
	protected void tick()
	{
		if(!wasTriggered)
		{
			for(int i=0; i<patched.size(); i++)
			{
				if(patched.get(i).wasTriggered())
				{
					wasTriggered = true;
					break;
				}
			}
		}
		
		if(wasTriggered)
		{
			for(int i=0; i<triggerables.size(); i++)
			{
				triggerables.get(i).trigger();
			}
		}
	}

	@Override
	public boolean wasTriggered()
	{
		return wasTriggered;
	}

	public void patch(Triggerable toTriggerable)
	{
		if(!triggerables.contains(toTriggerable))
		{
			triggerables.add(toTriggerable);
			getContainer().graph().setTerminal(this, true);
		}
	}
}
