package patchwork;

/**
 * Super-class for Inlets and Outlets.
 * @author Jack
 *
 */
public abstract class Let extends PatchNode
{
	final Component parent;
	
	public Let(Component parent)
	{
		super(parent.getContainer());
		this.parent = parent;
	}
	
	protected abstract void reset();
	
	@Override
	String getDefaultName()
	{
		return parent.getName() + ": " + super.getDefaultName();
	}
	
	@Override
	public void setName(String name)
	{
		super.setName(parent.getName() + ": " + name);
	}
	
	@Override
	protected abstract void tick();
}
