package patchwork;


public class FloatOutlet extends Outlet implements FloatValuable
{
	private float setValue;
	private float value;

	public FloatOutlet(Component parent)
	{
		super(parent);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void tick()
	{

		//System.out.println("TICK " + getName() + " " + value());
	}
	
	/**
	 * Set the value of the outlet.
	 * This should only be called from the tick() method of the unit this is an outlet of.
	 * @param value
	 */
	public void set(float value)
	{
		this.value = value;
	}

	@Override
	public float floatValue()
	{
		return value;
	}

	@Override
	protected void reset()
	{
		value = 0.0f;
	}

	@Override
	protected boolean createPatch(Outlet outlet)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
