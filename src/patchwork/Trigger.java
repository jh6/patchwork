package patchwork;

import java.util.ArrayList;
import java.util.List;

public class Trigger extends Tickable implements TriggerSource
{
	boolean wasTriggered;
	
	private final List<Triggerable> triggerables;
	
	public Trigger()
	{
		triggerables = new ArrayList<Triggerable>();
		
		Patchwork.INSTANCE.addPostTickable(this);
	}

	public void trigger()
	{
		wasTriggered = true;
	}
	
	@Override
	void tick()
	{
		wasTriggered = false;
	}
	
	public void patch(TriggerInlet toTriggerInlet)
	{
		PatchGraph.createObjectPatch(this, toTriggerInlet);
	}
	
	public void patch(Triggerable toTriggerable)
	{
		if(!triggerables.contains(toTriggerable))
		{
			triggerables.add(toTriggerable);
		}
	}

	@Override
	public boolean wasTriggered()
	{
		return wasTriggered;
	}
}
