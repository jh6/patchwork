package patchwork;

import java.util.ArrayList;
import java.util.List;


public class FloatInlet extends Inlet implements FloatValuable
{
	private float setValue;
	private float value;
	
	final List<FloatValuable> patched;

	public FloatInlet(Component parent)
	{
		super(parent);
		
		patched = new ArrayList<FloatValuable>();
	}
	
	@Override
	protected void reset() {}

	@Override
	protected void tick()
	{	
		value = 0f;
				
		for(int i=0; i<patched.size(); i++)
		{
			value += patched.get(i).floatValue();
		}
		
		value += setValue;
	}
	
	/**
	 * Set the base value of the inlet. This value will be summed with patched values.
	 * @param value
	 */
	public void set(float value)
	{
		setValue = value;
		
		reset();
		tick();
	}

	@Override
	public float floatValue()
	{
		return value;
	}

	@Override
	protected boolean createPatch(Object obj)
	{
		if(obj instanceof FloatValuable)
		{
			if(!patched.contains(obj))
			{
				patched.add((FloatValuable) obj);
			}
			
			return true;
		}
				
		return false;
	}
	
}
