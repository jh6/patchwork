package patchwork;

public interface Triggerable
{
	void trigger();
}
