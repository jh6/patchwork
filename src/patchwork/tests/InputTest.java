package patchwork.tests;

import patchwork.Patchwork;

public class InputTest
{
	public static void main(String[] args)
	{
		Patchwork pw = new Patchwork();
		
		pw.adc.channel(0).patch(pw.dac.channel(1));
		
		pw.start();
	}
}
