package patchwork.tests;

import patchwork.B;
import patchwork.F;
import patchwork.Patchwork;
import patchwork.Waveform;
import patchwork.gui.PWGUI;
import patchwork.signal.Signal.ChannelMode;
import patchwork.units.generators.Oscillator;
import patchwork.units.logic.AndGate;
import patchwork.units.logic.BRandom;
import patchwork.units.logic.OrGate;
import patchwork.units.logic.SignalSwitch;
import patchwork.units.logic.XorGate;

public class LogicTest
{
	public static void main(String[] args)
	{
		Patchwork pw = new Patchwork();
		PWGUI.show(pw);
		
		Oscillator osc = new Oscillator(Waveform.TRIANGLE, pw);
		SignalSwitch s = new SignalSwitch(ChannelMode.STEREO, pw);
		
		AndGate and = new AndGate(pw);
		OrGate or = new OrGate(pw);
		XorGate xor = new XorGate(pw);
		
		new F(150f).patch(osc.frequency);
		
		xor.patch(s.state);
		
		new B(false).patch(xor.a);
		new B(false).patch(xor.b);
		
		osc.patch(s).patch(pw.dac);
	}

}
