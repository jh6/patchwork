package patchwork.tests;

import patchwork.F;
import patchwork.Patchwork;
import patchwork.Waveform;
import patchwork.gui.PWGUI;
import patchwork.signal.Signal.ChannelMode;
import patchwork.units.generators.Oscillator;
import patchwork.units.math.Amplifier;

public class WaveformTest
{

	public static void main(String[] args)
	{
		Patchwork pw = new Patchwork();
		PWGUI.show(pw);
		
		Oscillator osc = new Oscillator(Waveform.TRIANGLE, ChannelMode.MONO, pw);
		
		new F(450f).patch(osc.frequency);
		
		Amplifier amp = new Amplifier(pw);
		new F(.3f).patch(amp.multiplier);
		
		osc.patch(amp).patch(pw.dac);
	}
}
