package patchwork.tests;

import patchwork.Patchwork;
import patchwork.Timer;
import patchwork.Waveform;
import patchwork.gui.PWGUI;
import patchwork.units.FSampleDelay;
import patchwork.units.generators.Line;
import patchwork.units.generators.Oscillator;
import patchwork.units.math.FRandom;

public class LineTest
{
	public static void main(String[] args)
	{
		Patchwork pw = new Patchwork();
		PWGUI.show(pw);
		
		Oscillator osc = new Oscillator(Waveform.TRIANGLE, pw);
		FRandom freqTarget = new FRandom(50f, 550f, pw);
		Line freqLine = new Line(pw);
		
		freqLine.patch(freqLine.start);
		freqLine.patch(osc.frequency);
		freqTarget.patch(freqLine.end);
		
		freqLine.duration.set(4410);
		freqLine.onComplete.patch(freqLine);
		freqLine.onComplete.patch(freqTarget);

		
		freqTarget.trigger();
		freqLine.trigger();

		osc.patch(pw.dac);
	}
}
