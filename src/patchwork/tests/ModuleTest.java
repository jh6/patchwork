package patchwork.tests;

import patchwork.F;
import patchwork.Module;
import patchwork.PatchContainer;
import patchwork.Patchwork;
import patchwork.SignalInlet;
import patchwork.SignalOutlet;
import patchwork.Waveform;
import patchwork.audioengines.JavaSoundAudioEngine;
import patchwork.signal.Signal.ChannelMode;

import patchwork.units.generators.Oscillator;
import patchwork.units.math.Amplifier;

public class ModuleTest
{
	private static class FMModule extends Module
	{
		public final SignalInlet frequency;
		public final SignalInlet modFrequency;
		public final SignalInlet modIndex;
		public final SignalInlet amp;
		
		private final SignalOutlet out;

		public FMModule(PatchContainer parent)
		{
			super(parent);
			
			frequency = new SignalInlet(ChannelMode.MONO, this);
			modFrequency = new SignalInlet(ChannelMode.MONO, this);
			modIndex = new SignalInlet(ChannelMode.MONO, this);
			amp = new SignalInlet(ChannelMode.MONO, this);
			out = new SignalOutlet(ChannelMode.MONO, this);
			
			setDefaultOutlet(out);
			
			// Main oscillator.
			Oscillator osc = new Oscillator(Waveform.SINE, ChannelMode.MONO, this);
			frequency.patch(osc.frequency);
			
			// Modulator.
			Oscillator mod = new Oscillator(Waveform.SINE, ChannelMode.MONO, this);
			modFrequency.patch(mod.frequency);
			
			// Amp to apply modIndex.
			Amplifier modAmp = new Amplifier(this);
			modIndex.patch(modAmp.multiplier);

			// Output amp
			Amplifier outputAmp = new Amplifier(this);
			amp.patch(outputAmp.multiplier);

			
			// Patch modulator
			mod.patch(modAmp).patch(osc.frequency);
			
			// Patch oscillator
			osc.patch(outputAmp).patch(out);
		}
		
	}
	
	public static void main(String[] args)
	{
		F baseHz = new F(1500f);
		F modHz = new F(.5f);
		F modIndex = new F(1000f);
		F amFrequency = new F(.05f);
		F amModIndex = new F(.2f);
		
		Patchwork pw = new Patchwork(new JavaSoundAudioEngine());
	
		FMModule fm = new FMModule(pw);
		baseHz.patch(fm.frequency);
		modHz.patch(fm.modFrequency);
		modIndex.patch(fm.modIndex);
		
		
		Oscillator am = new Oscillator(Waveform.SINE, ChannelMode.MONO, pw);
		amFrequency.patch(am.frequency);
		
		Amplifier amAmp = new Amplifier(pw);
		amModIndex.patch(amAmp.multiplier);
		
		am.patch(amAmp).patch(fm.amp);
		
		fm.patch(pw.dac);

		
		// Beautiful music :D
		pw.start();
	}
}
