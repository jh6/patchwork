package patchwork.tests;

import patchwork.F;
import patchwork.Patchwork;
import patchwork.Waveform;
import patchwork.audioengines.JavaSoundAudioEngine;
import patchwork.signal.Signal.ChannelMode;

import patchwork.units.generators.Oscillator;
import patchwork.units.math.Amplifier;

public class FMTest
{
	public static void main(String[] args)
	{
		Patchwork pw = new Patchwork(new JavaSoundAudioEngine());

		F baseHz = new F(1500f);
		F modHz = new F(1f);
		F modIndex = new F(1000f);
		F outputVolume = new F(.2f);
		
		// Main oscillator.
		Oscillator osc = new Oscillator(Waveform.SINE, ChannelMode.MONO, pw);
		baseHz.patch(osc.frequency);
		
		// Modulator.
		Oscillator mod = new Oscillator(Waveform.SINE, ChannelMode.MONO, pw);
		modHz.patch(mod.frequency);
		
		// Amp to apply modIndex.
		Amplifier modAmp = new Amplifier(pw);
		modIndex.patch(modAmp.multiplier);

		// Output amp
		Amplifier outputAmp = new Amplifier(pw);
		outputVolume.patch(outputAmp.multiplier);

		
		// Patch modulator
		mod.patch(modAmp).patch(osc.frequency);
		
		// Patch oscillator
		osc.patch(outputAmp).patch(pw.dac);

		
		// Beautiful music :D
		pw.start();
	}
}
