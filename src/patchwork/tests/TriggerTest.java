package patchwork.tests;

import patchwork.F;
import patchwork.Patchwork;
import patchwork.Timer;
import patchwork.Trigger;
import patchwork.Waveform;
import patchwork.gui.PWGUI;
import patchwork.units.generators.Oscillator;
import patchwork.units.math.FRandom;

public class TriggerTest
{
	public static void main(String[] args)
	{
		Patchwork pw = new Patchwork();
		PWGUI.show(pw);
		
		Trigger t = new Trigger();
		FRandom frand = new FRandom(500.0f, 800.0f, pw);
		
		Oscillator osc = new Oscillator(Waveform.TRIANGLE, pw);
		
		osc.patch(pw.dac);
		
		frand.patch(osc.frequency);
		
		Timer timer = new Timer(pw);
		timer.interval.set(4410);
		
		timer.patch(frand);
	}
}
