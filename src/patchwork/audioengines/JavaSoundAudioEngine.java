package patchwork.audioengines;

import java.nio.ByteBuffer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.DataLine.Info;
import javax.sound.sampled.TargetDataLine;

import patchwork.AudioEngine;

/**
 * AudioEngine using JavaSound.
 * @author Jack
 *
 */
public class JavaSoundAudioEngine extends AudioEngine
{
	private int sampleRate = 44100;
	private int bitsPerSample = 16;
	private int bytesPerSample = 2;

	private int inputBufferSizeInBytes;
	private int outputBufferSizeInFrames = 44100;
	private int outputBufferSizeInBytes = outputBufferSizeInFrames * bytesPerSample * numOutputChannels();
	
	private AudioFormat format;
	
	boolean isRunning;
	
	private ByteBuffer inputBuffer;
	private ByteBuffer outputBuffer;
	
	private TargetDataLine lineIn;
	private SourceDataLine lineOut;

	public JavaSoundAudioEngine()
	{
		format = new AudioFormat(sampleRate, bitsPerSample, numOutputChannels(), true, true);
	}
	
	@Override
	protected void start()
	{
		DataLine.Info outputInfo = new DataLine.Info(SourceDataLine.class, format);
		DataLine.Info inputInfo = new DataLine.Info(TargetDataLine.class, format);
		
		try
		{
			lineOut = (SourceDataLine) AudioSystem.getLine(outputInfo);
			lineIn = (TargetDataLine) AudioSystem.getLine(inputInfo);
				
			lineOut.open(format, outputBufferSizeInBytes);
			lineIn.open(format);
		}
		catch(LineUnavailableException e)
		{
			e.printStackTrace();
		}
		
		inputBufferSizeInBytes = bytesPerSample * numOutputChannels();
		
		inputBuffer = ByteBuffer.allocate(inputBufferSizeInBytes);
		outputBuffer = ByteBuffer.allocate(outputBufferSizeInBytes);		
		
		lineIn.start();
		lineOut.start();
		
		isRunning = true;
		new Thread(new AudioRunnable()).start();
	}

	@Override
	protected void stop()
	{
		isRunning = false;
		
		lineIn.flush();
		lineIn.close();
		lineOut.flush();
		lineOut.close();
	}
	
	@Override
	protected float sampleRate()
	{
		return format.getFrameRate();
	}
	
	@Override
	protected int numOutputChannels()
	{
		return 2;
	}
	
	@Override
	protected int numInputChannels()
	{
		return 2;
	}
	
	private class AudioRunnable implements Runnable
	{
		@Override
		public void run()
		{
			float[] input = new float[numOutputChannels()];
			
			while(isRunning)
			{
//				lineIn.read(inputBuffer.array(), 0, inputBuffer.capacity());
//				
//				for(int i=0; i<numOutputChannels(); i++)
//				{
//					input[i] = (float) inputBuffer.getShort() / Short.MAX_VALUE;
//				}
				
				
				
				inputBuffer.clear();
				
				float[] output = process(input);
				
				for(int i=0; i<output.length; i++)
				{
					short s = (short) (output[i] * Short.MAX_VALUE);
					outputBuffer.putShort(s);
				}
				
				if(!outputBuffer.hasRemaining())
				{
					lineOut.write(outputBuffer.array(), 0, outputBuffer.capacity());
					outputBuffer.clear();
				}
			}
		}
	}
}
