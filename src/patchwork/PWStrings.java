package patchwork;

class PWStrings
{
	static String alreadyInstantiated()
	{
		return "There may only be one instance of Patchwork!";
	}
	
	static String invalidPatch(Let a, Let b)
	{
		return couldNotCreatePatch(a, b) + " : not a valid patch.";
	}
	
	static String notInSameContainer(Let a, Let b)
	{
		return couldNotCreatePatch(a, b) + " : not in the same PatchContainer.";
	}

	static String noDefaultInlet(PatchNode node, Component c)
	{
		return couldNotCreatePatch(node, c) + " : " + c.getName() + " has no default inlet.";
	}
	
	static String noDefaultOutlet(Component c, PatchNode node)
	{
		return couldNotCreatePatch(c, node) + " : " + c.getName() + " has no default outlet.";
	}
	
	static String noDefaultInlet(String a, String b)
	{
		return couldNotCreatePatch(a, b) + " : " + b + " has no default inlet.";
	}
	
	static String notAModuleOutlet(Outlet o, Outlet notModuleOutlet)
	{
		return couldNotCreatePatch(o, notModuleOutlet) + " : " + notModuleOutlet.getName()
				+ " is not the outlet to a module.";
	}
	
	static String notAModuleInlet(Inlet notModuleInlet, Inlet i)
	{
		return couldNotCreatePatch(notModuleInlet, i) + " : " + notModuleInlet.getName()
				+ " is not the outlet to a module.";
	}
	
	static String outletNotInModule(Let o, Let moduleOutlet)
	{
		return couldNotCreatePatch(o, moduleOutlet) + " : " + o.getName()
				+ " is not in the module" + moduleOutlet.parent.getName();
	}
	
	static String inletNotInModule(Let moduleInlet, Let i)
	{
		return couldNotCreatePatch(moduleInlet, i) + " : " + i.getName()
				+ " is not in the module" + moduleInlet.parent.getName();
	}
	
	
	static String couldNotCreatePatch(PatchNode a, PatchNode b)
	{
		return couldNotCreatePatch(a.getName(), b.getName());
	}
	
	static String couldNotCreatePatch(String a, String b)
	{
		return "Could not create patch [" + a + "] => [" + b + "]";
	}

	
	static String couldNotSetDefaultLet(Let let)
	{
		String letType = "inlet";
		
		if(let instanceof Outlet)
		{
			letType = "outlet";
		}
		
		return "Could not set default " + letType + " in " + let.parent.getName() + " : "
			+ let.getName() + " is not an " + letType + " to " + let.parent.getName() + ".";
	}
}
