package patchwork;

import java.util.HashMap;
import java.util.Map;

import patchwork.signal.Signal.ChannelMode;

public final class Adc extends Component
{
	private final SignalOutlet out;
	
	private final Map<Integer, SignalOutlet> channels;
	
	private final int numChannels;
	private float[] input;

	Adc(int numChannels, Patchwork patchwork)
	{
		super(patchwork);
				
		out = new SignalOutlet(ChannelMode.STEREO, this);
		channels = new HashMap<Integer, SignalOutlet>();
		
		this.numChannels = numChannels;
	}
	
	public SignalOutlet channel(int channelIndex)
	{
		if(!channels.containsKey(channelIndex))
		{
			channels.put(channelIndex, new SignalOutlet(ChannelMode.MONO, this));
		}
		
		return channels.get(channelIndex);
	}
	
	void setInput(float[] input)
	{
		this.input = input;
	}

	@Override
	protected void process()
	{
		out.set(input);
		
		for(int i=0; i< numChannels; i++)
		{
			if(channels.containsKey(i))
			{
				channels.get(i).set(input[i]);
			}
		}
	}
	
	public void patch(Inlet toInlet)
	{
		PatchGraph.createPatch(out, toInlet);
	}
	
	public Unit patch(Unit toUnit)
	{
		PatchGraph.createPatch(out, toUnit);
		
		return toUnit;
	}
	
	public void patch(Dac toDac)
	{
		PatchGraph.createPatch(out, toDac);
	}
}
