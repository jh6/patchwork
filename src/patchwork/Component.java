package patchwork;

import java.util.ArrayList;
import java.util.List;

abstract class Component extends PatchNode
{
	Inlet defaultInlet;
	Outlet defaultOutlet;
	
	private final List<Inlet> inlets;
	private final List<Outlet> outlets;

	private final PatchContainer parent;
	
	Component(PatchContainer parent)
	{
		super(parent);
		
		inlets = new ArrayList<Inlet>();
		outlets = new ArrayList<Outlet>();
		
		this.parent = parent;
	}
	
	void addInlet(Inlet newInlet)
	{
		inlets.add(newInlet);
		parent.graph().createDependency(this, newInlet);
	}
	
	void addOutlet(Outlet newOutlet)
	{
		outlets.add(newOutlet);
		parent.graph().createDependency(newOutlet, this);
	}
	
	protected final void setDefaultInlet(Inlet inlet)
	{
		if(inlet.parent != this)
		{
			throw new RuntimeException(PWStrings.couldNotSetDefaultLet(inlet));
		}
		
		defaultInlet = inlet;
	}
	
	protected final void setDefaultOutlet(Outlet outlet)
	{
		if(outlet.parent != this)
		{
			throw new RuntimeException(PWStrings.couldNotSetDefaultLet(outlet));
		}
		
		defaultOutlet = outlet;
	}
	
	@Override
	final void tick()
	{		
		for(int i=0; i<outlets.size(); i++)
		{
			outlets.get(i).reset();
		}
		
		process();
		
		for(int i=0; i<inlets.size(); i++)
		{
			inlets.get(i).reset();
		}
	}
	
	protected abstract void process();
}
