package patchwork;

import java.util.ArrayList;
import java.util.List;

import patchwork.audioengines.JavaSoundAudioEngine;

public final class Patchwork implements PatchContainer
{
	static Patchwork INSTANCE;
	
	public final Adc adc;
	public final Dac dac;
	
	private final PatchGraph graph;
	private final List<Tickable> postTickables;
	
	private final AudioEngine audioEngine;
	
	private boolean isRunning;
		
	public Patchwork(AudioEngine audioEngine)
	{		
		if(INSTANCE != null)
		{
			throw new RuntimeException(PWStrings.alreadyInstantiated());
		}
		
		INSTANCE = this;
		
		graph = new PatchGraph(this);
		postTickables = new ArrayList<Tickable>();
		
		this.audioEngine = audioEngine;
		audioEngine.init(this);
		
		adc = new Adc(audioEngine.numInputChannels(), this);
		dac = new Dac(audioEngine.numOutputChannels(), this);
	}
	
	public Patchwork()
	{
		this(new JavaSoundAudioEngine());
	}
	
	void tick()
	{
		graph.tick();
		
		for(int i=0; i<postTickables.size(); i++)
		{
			postTickables.get(i).tick();
		}
	}
	
	public void start()
	{
		audioEngine.start();
		isRunning = true;
	}
	
	public void stop()
	{
		audioEngine.stop();
		isRunning = false;
	}

	public float sampleRate()
	{
		return audioEngine.sampleRate();
	}

	@Override
	public PatchGraph graph()
	{
		return graph;
	}
	
	void addPostTickable(Tickable t)
	{
		postTickables.add(t);
	}
	
	public boolean getIsRunning()
	{
		return isRunning;
	}
}
