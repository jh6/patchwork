package patchwork;


public abstract class Inlet extends Let
{
	public Inlet(Component parent)
	{
		super(parent);
		
		parent.addInlet(this);
	}
	
	protected abstract boolean createPatch(Object obj);
	
	final void patch(Inlet toInlet)
	{
		PatchGraph.createPatch(this, toInlet);
	}
}
